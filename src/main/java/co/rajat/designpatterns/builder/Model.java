/*
 * This file is part of Design Patterns.
 *
 * Design Patterns is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Design Patterns is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Design Patterns.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Represents a class to be built using the Builder pattern.
 *
 * @author Rajat Arora
 * @since 0.0.1
 * @see co.rajat.designpatterns.builder.ModelBuilder
 */
package co.rajat.designpatterns.builder;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode @ToString
class Model {

    @Getter private final int anInt;
    @Getter private final String aString;
    @Getter private final double aDouble;
    @Getter private final String name;

    Model(ModelBuilder modelBuilder) {
        this.name = modelBuilder.name;
        this.anInt = modelBuilder.anInt;
        this.aString = modelBuilder.aString;
        this.aDouble = modelBuilder.aDouble;
    }

}
