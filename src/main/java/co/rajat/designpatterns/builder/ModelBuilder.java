/*
 * This file is part of Design Patterns.
 *
 * Design Patterns is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Design Patterns is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Design Patterns.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Implements the Builder pattern for {@link co.rajat.designpatterns.builder.Model}
 *
 * @author Rajat Arora
 * @since 0.0.1
 * @see co.rajat.designpatterns.builder.Model
 */

package co.rajat.designpatterns.builder;

class ModelBuilder {

    int anInt;
    String aString;
    double aDouble;
    final String name;

    ModelBuilder(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        this.name = name;
    }

    ModelBuilder withAnInt(int anInt) {
        this.anInt = anInt;
        return this;
    }

    ModelBuilder withAString(String aString) {
        this.aString = aString;
        return this;
    }

    ModelBuilder withADouble(double aDouble) {
        this.aDouble = aDouble;
        return this;
    }

    Model build() {
        return new Model(this);
    }
}
