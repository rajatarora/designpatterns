/*
 * This file is part of Design Patterns.
 *
 * Design Patterns is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Design Patterns is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Design Patterns.  If not, see <https://www.gnu.org/licenses/>.
 */

package co.rajat.designpatterns.builder;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for the Builder pattern.
 *
 * @author Rajat Arora
 * @since 0.0.1
 * @see Model
 * @see ModelBuilder
 */
public class ModelBuilderTest {

    /**
     * Checks for expected {@link IllegalArgumentException} when {@link ModelBuilder} is initialized with a null name.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testNullName() {
        new ModelBuilder(null);
    }

    /**
     * Checks whether the {@link Model} is built properly when constructed using {@link ModelBuilder}
     */
    @Test
    public void testBuilder() {
        String name = "Harry Potter";
        String aString = "Voldemort";
        int anInt = 7;
        double aDouble = 3.6d;

        Model model = new ModelBuilder(name)
                .withADouble(aDouble)
                .withAnInt(anInt)
                .withAString(aString)
                .build();

        assertNotNull(model);
        assertNotNull(model.toString());
        assertEquals(name, model.getName());
        assertEquals(aString, model.getAString());
        assertEquals(anInt, model.getAnInt());
        assertEquals(aDouble, model.getADouble(), 0.001d);
    }
}